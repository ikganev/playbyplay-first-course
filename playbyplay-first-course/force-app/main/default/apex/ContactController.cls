public with sharing class ContactController {
    @AuraEnabled(cacheable=true)
    public static List<Contact> getContactList(String accId) {
        return [SELECT Id, Name ,Email, Phone, Account.Name from Contact where AccountId = :accId];
    }
}

