import { LightningElement, api } from 'lwc';

export default class Person extends LightningElement {
    @api
    firstName = 'Chuck';

    @api
    lastName = 'Liddell';
}