import { LightningElement } from 'lwc';

export default class HelloForEach extends LightningElement {
    contacts = [
        {
            Id: '0000000000001',
            Name: 'Van Roy',
            Title: 'VP of Engineering',
        },
        {
            Id: '0000000000002',
            Name: 'Ivan Robertos',
            Title: 'VP of Sales',
        },
        {
            Id: '0000000000003',
            Name: 'Iban Hoy',
            Title: 'CEO',
        },
    ];
}