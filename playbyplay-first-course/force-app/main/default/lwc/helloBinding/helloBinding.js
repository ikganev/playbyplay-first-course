import { LightningElement, track, api } from 'lwc';

export default class HelloBinding extends LightningElement {
    @api greeting = 'World';

    handleChange(event) {
        this.greeting = event.target.value;
        console.log(this.greeting);
    }
} 

// let lipstick = function(color) {
//     return function(target) {
//         target.lips = color;
//     }
//   }
//   let earings = function(target) {
//       target.hasEarings = "Yes, she does.";
//   }
  
//   @earings
//   @lipstick('black')
//   class Girl {
    
//   }
  
//   alert(`Her lips are ${Girl.lips}. Is she wear earings? ${Girl.hasEarings}`);
  