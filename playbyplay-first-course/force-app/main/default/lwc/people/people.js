import { LightningElement, track } from 'lwc';

export default class People extends LightningElement {
    peopleParsed = false;

    @track
    record = [];

    renderCallBack() {
        if(this.peopleParsed) return;

        this.peopleParsed = true;
        Array.from(this.querySelectorAll('c-person')).forEach(person => 
            console.log(`Found: ${person.firstName} ${person.lasttName}`),
            this.records.push({
                key : person.firstName + person.lasttName,
                firstName : person.firstName,
                lasttName : person.lasttName
        }));
    }
}